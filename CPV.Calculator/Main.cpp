#include <iostream>
#include <conio.h>

using namespace std;

float Add(float num1, float num2)
{
	return num1 + num2;
}
float Subtract(float num1, float num2)
{
	return num1 - num2;
}
float Multiply(float num1, float num2)
{
	return num1 * num2;
}
bool Divide(float num1, float num2, float &answer)
{
	if (num2 == 0) { return false; }
	else {
		return answer = num1 / num2;
	}
}

int main()
{
	char op;
	float num1, num2;
	float answer;

	

	while (true)
	{
		cout << "Enter character (+-*/) or type \"q\" to quit.";
		cin >> op;

		if (op == 'q')
		{
			return 0;
		}
		cout << "Please enter two positive numbers - ";
		cin >> num1 >> num2;
		if (op == '+')
		{
			cout << Add(num1, num2) << "\n";
		}
		else if (op == '-')
		{
			cout << Subtract(num1, num2) << "\n";
		}
		else if (op == '*')
		{
			cout << Multiply(num1, num2) << "\n";
		}
		else if (op == '/')
		{
			if (Divide(num1, num2, answer)) {
				cout << answer << "\n";
			}
			else {
				cout << "Cannot divide by zero!!" << "\n";
			}
		}
		else
		{
			cout << "Wrong operator!";
			cout << "\n";
		}
		

	}
	return 0;
}
